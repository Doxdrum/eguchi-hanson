load_package excalc;

%on factor;
%on reduced;

off precise;

spacedim 4;
indexrange 0,1,2,3;

pform {f,h,w1,w2,w3,w4,w5,w6,w7,w8,phi,xi,riemann(a,b,c,d),ricci(a,b),rscal,kret}=0,
      om(a,b)=1,
      {curv(a,b),curvdual(a,b),tor(a)}=2,
      {eome(a),eomw(a,b),tau(a),J,CSp}=3,
      {eomp,pontryagin}=4;

index_symmetries curv(a,b):antisymmetric,
                 om(a,b):antisymmetric,
                 curvdual(a,b):antisymmetric,
                 eomw(a,b):antisymmetric,
                 riemann(k,l,m,n): antisymmetric in {k,l},{m,n}
				   symmetric in {{k,l},{m,n}},
                 ricci(a,b): symmetric;

fdomain f=f(r),
        h=h(r),
        phi=phi(r),
        w1=w1(r),
        w2=w2(r),
        w3=w3(r),
        w4=w4(r),
        w5=w5(r),
        w6=w6(r),
        w7=w7(r),
        w8=w8(r),
        xi=xi(r);

%%%%% Self-duality condition %%%%%%%%

w3 := w6;
w4 := -w5;
w1 := (sqrt(f)*r*w7+f-2)/(sqrt(f)*r);

%%%%%%%%% Solving the w feqs %%%%%%%%%

w2 := 0;
w6 := 0;
w8 := 0;

%%%%%%%% Eguchi-Hanson metric %%%%%%%%

coframe e(0)          =sqrt(f)*(r/2)*(d t + cos(x)*d y),
        e(1)          = (1/sqrt(h))*d r,
        e(2)          = (r/2)*d x,
        e(3)          =(r/2)*sin(x)*d y
        with metric g = e(0)*e(0) + e(1)*e(1) + e(2)*e(2) + e(3)*e(3); 

frame ee;

factor e,^;

%%% Connection %%%%

riemannconx omr;

om(0,1) := w1*e(0) + w2*e(1);
om(0,2) := w3*e(2) + w4*e(3);
om(0,3) := -w4*e(2) + w3*e(3);
om(1,2) := w5*e(2) + w6*e(3);
om(1,3) := -w6*e(2) + w5*e(3);
om(2,3) := w7*e(0) + w8*e(1) + omr(2,3);

%%% Curvature and torsion 2-form %%%

curv(a,b)     := d om(a,b) + om(a,-c)^om(c,b);
tor(a)        := d e(a) + om(a,-b)^e(b);
curvdual(a,b) := (1/2)*eps(a,b,c,d)*curv(-c,-d);

%%% Self duality %%%

curv(a,b) - curvdual(a,b);

%%% Tensors %%%%

riemann(a,b,-c,-d) := ee(-d) _| (ee(-c) _| curv(a,b))$
ricci(-a,-b)       := riemann(m,-a,-m,-b)$

%%% Curvature invariants %%%

rscal      := ricci(a,-a);
kret       := riemann(-a,-b,-c,-d)*riemann(a,b,c,d);
pontryagin := curv(a,b)^curv(-b,-a);
CSp        := om(a,-b) ^ (curv(b,-a) - 1/3*om(b,-c) ^ om(c,-a));

%%%% Field equations with cosmological constant %%%% 

J       := # d phi - alpha/(4*k)*CSp;
tau(-a) := -(1/2)*(d phi^(# (d phi^e(-a))) + (ee(-a) _| d phi)*(# d phi));

off nat;
out "Eguchi-Hanson-out"; 

eome(-a)    := eps(-a,-b,-c,-d)*curv(b,c)^e(d) - 2*k*tau(-a);
eomw(-a,-b) := eps(-a,-b,-c,-d)* tor(c)^e(d) - alpha* d phi ^ curv(-a,-b);
eomp        := (d # d phi) - alpha/(4*k)*curv(a,b)^curv(-b,-a);

write "end";
shut "Eguchi-Hanson-out";
on nat;

%%%%%%%%% TESTS AND CALCULATIONS %%%%%%%%%

%%% self duality %%%
%% (d phi ^ #(d phi ^e(-a))) - (d phi) ^ #(d phi ^e(-a));
%% curv(a,b) - curvdual(a,b);
pontryagin - d CSp;
eomp - d J;
%% eome(-a) - 2*(d tor(-a) - om(b,-a)^tor(-b) - k*tau(-a));
%% d tor(-a) - om(b,-a)^tor(-b) - curv(-a,-b)^e(b);
%% eps(-a,-b,-c,-d)*curv(b,c)^e(d) - 2*curv(-a,-b)^e(b);
%% eps(-a,-b,-c,-d)*curv(b,c)^e(d) - 2*(d tor(-a) - om(b,-a)^tor(-b));

end$
